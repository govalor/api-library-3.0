﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class CourceDefinitionNW
    {
        public class CourseAlternate
        {
            public string nwId { get; set; }
            public string Course { get; set; }
            public string nwNateId { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public int InternalSeq { get; set; }
            public string AlternateCourse { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public object Credits { get; set; }
            public string Subject { get; set; }
            public bool Elective { get; set; }
            public bool Inactive { get; set; }
            public bool PassFail { get; set; }
            public bool Weighted { get; set; }
            public bool YearLong { get; set; }
            public bool znwLocked { get; set; }
            public bool CoreCourse { get; set; }
            public string CourseCode { get; set; }
            public string CourseName { get; set; }
            public string CourseType { get; set; }
            public string Department { get; set; }
            public List<string> GradeLevels { get; set; }
            public int MaxClassSize { get; set; }
            public int MinClassSize { get; set; }
            public bool OnlineCourse { get; set; }
            public bool AdditionalFee { get; set; }
            public List<string> ScheduleTerms { get; set; }
            public bool CourseRequired { get; set; }
            public bool OfferedNextYear { get; set; }
            public List<object> ApprovedTeachers { get; set; }
            public List<CourseAlternate> CourseAlternates { get; set; }
            public int MaxClassesPerTerm { get; set; }
            public int MinClassesPerTerm { get; set; }
            public bool PrintOnTranscript { get; set; }
            public string SchedulingPriority { get; set; }
            public List<object> CoursePrerequisites { get; set; }
            public List<object> CourseRoomRestrictions { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
