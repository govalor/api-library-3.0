﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class CourceSectionsNW
    {
        public class ZnwWorkflowInstance
        {
            public string WorkflowName { get; set; }
            public object WorkflowType { get; set; }
            public string WorkflowTable { get; set; }
            public int WorkflowVersion { get; set; }
            public object WorkflowApprovalId { get; set; }
            public string WorkflowInstanceId { get; set; }
            public string WorkflowStateLookup { get; set; }
            public string WorkflowCurrentState { get; set; }
            public string WorkflowCurrentStateType { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Term { get; set; }
            public string Block { get; set; }
            public string Course { get; set; }
            public string Teacher { get; set; }
            public bool Inactive { get; set; }
            public bool znwLocked { get; set; }
            public int SectionName { get; set; }
            public string AcademicYear { get; set; }
            public string AcademicYearTerm { get; set; }
            public string CourseCodeSection { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public int NumberOfEnrolledStudents { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string RoomNumber { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
