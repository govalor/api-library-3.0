﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class StudentGrades
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string Term { get; set; }
            public string Course { get; set; }
            public int Credits { get; set; }
            public string EndDate { get; set; }
            public string Student { get; set; }
            public int Sequence { get; set; }
            public bool znwLocked { get; set; }
            public string CourseType { get; set; }
            public string GradeLetter { get; set; }
            public double GradeNumber { get; set; }
            public string GradeStatus { get; set; }
            public string AcademicYear { get; set; }
            public string GradingScale { get; set; }
            public string CourseSection { get; set; }
            public string GradingPeriod { get; set; }
            public bool CreditsAwarded { get; set; }
            public string TransferSchool { get; set; }
            public bool FromIntegration { get; set; }
            public string AcademicYearTerm { get; set; }
            public bool IntegrationError { get; set; }
            public bool IsTransferSchool { get; set; }
            public bool SendNotification { get; set; }
            public bool PrintOnTranscript { get; set; }
            public string TransferCourseName { get; set; }
            public string TransferCourseTerm { get; set; }
            public bool GradeRedemptionUsed { get; set; }
            public int HistoricalGradeLevel { get; set; }
            public bool UseInGPACalculations { get; set; }
            public string GradeEnrollmentStatus { get; set; }
            public bool CreditsAwardedOverride { get; set; }
            public string TransferCourseAcademicYear { get; set; }
            public string GraduationRequirementSubject { get; set; }
            public string GradeEnrollmentStatusGrouping { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string TransferCourseType { get; set; }
            public double? TransferCourseCredits { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
