﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    public class NWDonationFundsDetail
    {
        public class DonationAmount
        {
            public string CurrencyCode { get; set; }
            public long CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class ErrorMessage
        {
            public string ErrorCode { get; set; }
            public string ErrorType { get; set; }
            public string ErrorDescription { get; set; }
        }

        public class WorkflowErrorObject
        {
            public List<ErrorMessage> ErrorMessages { get; set; }
        }

        public class ZnwWorkflowInstance
        {
            public string WorkflowName { get; set; }
            public object WorkflowType { get; set; }
            public string WorkflowTable { get; set; }
            public int WorkflowVersion { get; set; }
            public object WorkflowApprovalId { get; set; }
            public string WorkflowInstanceId { get; set; }
            public string WorkflowStateLookup { get; set; }
            public string WorkflowCurrentState { get; set; }
            public string WorkflowCurrentStateType { get; set; }
            public WorkflowErrorObject WorkflowErrorObject { get; set; }
        }

        public class SolicitorName
        {
            public string NameName { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
        }

        public class DonorName
        {
            public string NameName { get; set; }
            public string FormatUsed { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
            public string NameTitleBeforeName { get; set; }
            public string NamePrefixKeyNames { get; set; }
        }

        public class QPQIRSValue
        {
            public string CurrencyCode { get; set; }
            public int CurrencyValue { get; set; }
            public int CurrencyDecimals { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Donor { get; set; }
            public string FundID { get; set; }
            public string Company { get; set; }
            public bool znwLocked { get; set; }
            public string nwHeaderId { get; set; }
            public DonationAmount DonationAmount { get; set; }
            public string OrganizationalUnit { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public int DonationReceiptNumber { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string FundID_FundName { get; set; }
            public object FundEvent_EventName { get; set; }
            public SolicitorName Solicitor_Name { get; set; }
            public DonorName Donor_Name { get; set; }
            public QPQIRSValue QPQIRSValue { get; set; }
            public string QPQGoodsOrServices { get; set; }
            public string DonationMemo { get; set; }
            public object Appeal { get; set; }
            public object Activity { get; set; }
            public object Campaign { get; set; }
            public object znwOwner { get; set; }
            public object FundEvent { get; set; }
            public string Solicitor { get; set; }
            public object InternalSeq { get; set; }
            public object nwExternalId { get; set; }
            public object znwSparseOwner { get; set; }
            public string StudentActivity { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}