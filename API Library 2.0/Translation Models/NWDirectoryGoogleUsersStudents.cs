﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class NWDirectoryGoogleUsersStudents
    {
        public class Name
        {
            public string NameName { get; set; }
            public string FormatUsed { get; set; }
            public string NameKeyNames { get; set; }
            public string NameNameInverted { get; set; }
            public string NamePrefixKeyNames { get; set; }
            public string NameTitleBeforeName { get; set; }
            public string NameNamesBeforeKeyNames { get; set; }
            public string NameNamesAfterKeyNames { get; set; }
        }

        public class PrimaryAddress
        {
            public string FormatUsed { get; set; }
            public string AddressCity { get; set; }
            public string AddressFull { get; set; }
            public string AddressState { get; set; }
            public string AddressStreetName { get; set; }
            public string AddressCountry { get; set; }
            public string AddressStreetType { get; set; }
            public string AddressStreetNumber { get; set; }
            public string AddressStreetDirection { get; set; }
        }

        public class Logo
        {
            public string PictureLarge { get; set; }
            public string PictureSmall { get; set; }
            public string PictureImageName { get; set; }
            public string AttachmentGroupId { get; set; }
        }

        public class ZnwWorkflowInstance
        {
            public bool WorkflowChanged { get; set; }
        }

        public class AssignedRole
        {
            public string Role { get; set; }
            public object nwId { get; set; }
            public object Description { get; set; }
            public object InternalSeq { get; set; }
            public object ExpirationDate { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public int Age { get; set; }
            public Name Name { get; set; }
            public List<object> Emails { get; set; }
            public string Gender { get; set; }
            public List<object> Phones { get; set; }
            public int ClassOf { get; set; }
            public bool Deceased { get; set; }
            public bool Inactive { get; set; }
            public string Nickname { get; set; }
            public bool Anonymous { get; set; }
            public string BirthDate { get; set; }
            public int ContactID { get; set; }
            public string Ethnicity { get; set; }
            public List<object> HoldCodes { get; set; }
            public List<object> Languages { get; set; }
            public bool znwLocked { get; set; }
            public int GradeLevel { get; set; }
            public bool W9Provided { get; set; }
            public string AlternateID { get; set; }
            public object ContactRole { get; set; }
            public string ContactType { get; set; }
            public bool NonResident { get; set; }
            public List<object> SocialMedia { get; set; }
            public string PrimaryEmail { get; set; }
            public string PrimaryPhone { get; set; }
            public bool TrackBudgets { get; set; }
            public string nwExternalId { get; set; }
            public int GradeEnrolled { get; set; }
            public bool TwoFactorAuth { get; set; }
            public List<object> Accommodations { get; set; }
            public bool AllowBasicAuth { get; set; }
            public List<object> Certifications { get; set; }
            public bool CustomerOnHold { get; set; }
            public string GraduationDate { get; set; }
            public PrimaryAddress PrimaryAddress { get; set; }
            public bool SupplierOnHold { get; set; }
            public bool FromIntegration { get; set; }
            public bool PermissionDrive { get; set; }
            public bool TransferStudent { get; set; }
            public string EnrollmentStatus { get; set; }
            public string EnrollmentEndDate { get; set; }
            public string EligibilityStatus1 { get; set; }
            public string EligibilityStatus2 { get; set; }
            public bool ManufacturingPlant { get; set; }
            public List<string> ContactRoleGrouping { get; set; }
            public string ContactTypeGrouping { get; set; }
            public string EnrollmentStartDate { get; set; }
            public bool Form1099MISCEligible { get; set; }
            public List<object> OtherSchoolsAttended { get; set; }
            public bool InventoryOrganization { get; set; }
            public bool PurchaseOrderRequired { get; set; }
            public bool ProbationaryPeriodUsed { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public object SocialSecurityNumber { get; set; }
            public object VisaNumber { get; set; }
            public object PassportNumber { get; set; }
            public Logo Logo { get; set; }
            public string StudentID { get; set; }
            public List<object> NoteHistory { get; set; }
            public bool? Form1099Exist { get; set; }
            public List<object> ContactHistory { get; set; }
            public List<object> FundraisingMoves { get; set; }
            public string DefaultNameFormat { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public string GPAEligibilityStatus { get; set; }
            public string StudentRestrictionStatus { get; set; }
            public string FullName { get; set; }
            public string Password { get; set; }
            public string UserName { get; set; }
            public string BirthCity { get; set; }
            public string GivenName { get; set; }
            public string BirthState { get; set; }
            public string FamilyName { get; set; }
            public string FormalName { get; set; }
            public string Salutation { get; set; }
            public string DefaultZone { get; set; }
            public int? StudentRank { get; set; }
            public string BirthCountry { get; set; }
            public List<AssignedRole> AssignedRoles { get; set; }
            public double? GPAPercentile { get; set; }
            public string EmployeeEndDate { get; set; }
            public string IsNextworldUser { get; set; }
            public string CustomerCurrency { get; set; }
            public string DefaultLifecycle { get; set; }
            public string ProbationaryTerm { get; set; }
            public string SupplierCurrency { get; set; }
            public string AttachmentGroupId { get; set; }
            public string EmployeeStartDate { get; set; }
            public string EligibilityStatus3 { get; set; }
            public double? WeightedCumulative { get; set; }
            public double? UnweightedCumulative { get; set; }
            public string CustomerClassification { get; set; }
            public string ActivityEligibilityRule { get; set; }
            public string CommunicationPreference { get; set; }
            public DateTime? ProbationaryPeriodStamp { get; set; }
            public string ProbationaryAcademicYear { get; set; }
            public string ProbationaryPeriodGPAType { get; set; }
            public bool? EligibilityGracePeriodUsed { get; set; }
            public string ProbationaryPeriodGPAWeighting { get; set; }
            public string ProbationaryPeriodGradingPeriod { get; set; }
            public object Notes { get; set; }
            public bool? Redemption { get; set; }
            public List<object> AdditionalBooks { get; set; }
            public bool? GradeReplacement { get; set; }
            public bool? ReportingOnlyUnit { get; set; }
            public List<object> DirectoryAddresses { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
