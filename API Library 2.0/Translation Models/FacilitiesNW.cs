﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class FacilitiesNW
    {
        public class ZnwWorkflowInstance
        {
            public bool WorkflowChanged { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public bool znwLocked { get; set; }
            public string Description { get; set; }
            public string FacilityName { get; set; }
            public string FacilityType { get; set; }
            public string FacilityFloor { get; set; }
            public int? FacilityCapacity { get; set; }
            public string FacilityLocation { get; set; }
            public bool FacilitySchedule { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public object nwExternalId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public object InternalSeq { get; set; }
            public string znwSparseOwner { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
