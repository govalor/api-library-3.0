﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    public class NWFund
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string FundName { get; set; }
            public string FundType { get; set; }
            public bool Inactive { get; set; }
            public bool znwLocked { get; set; }
            public List<object> FundMessage { get; set; }
            public List<object> FundInformation { get; set; }
            public string OrganizationalUnit { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public object nwExternalId { get; set; }
            public string FundRestrictions { get; set; }
            public string FundDescription { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
