﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_Library_2._0.Translation_Models;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Authenticators;

namespace API_Library_2._0
{
    public static class ActivityDefinition
    {
        //public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("integration.valor", "11Govalor01!");

        //Token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("integration.valor", "11Govalor01!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "ValorIntegration";
        private static string LIFECYCLE = "valor";

        public static NWActivityDefinition.RootObject genActivityDefinition(string nwid)
        {
            NWActivityDefinition.RootObject temp = new NWActivityDefinition.RootObject();

            //var client = new RestClient("https://master-api1.nextworld.net/v2/Activity/"+nwid);
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/Activity/" + nwid);
            var request = new RestRequest(Method.GET);
            //client.Authenticator = auth;

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            temp = JsonConvert.DeserializeObject<NWActivityDefinition.RootObject>(response.Content);

            return temp;
        }


    }
}
