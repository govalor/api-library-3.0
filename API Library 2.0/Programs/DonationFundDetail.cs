﻿using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public static class DonationFundDetail
    {
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");

        public static NWDonationFundsDetail.RootObject getDonationFundDetail(string nwid)
        {
            NWDonationFundsDetail.RootObject temp = new NWDonationFundsDetail.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution/"+nwid);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            temp = JsonConvert.DeserializeObject<NWDonationFundsDetail.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return temp;
        }
    }
}
