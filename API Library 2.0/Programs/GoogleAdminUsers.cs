﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminUsers
    {
        public static string URL_BASE_STUDENT = "https://master-api1.nextworld.net/v2/JVAdminGetStudents?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_FACULTY = "https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22faculty@valorchristian.com%22%7D%7D%5D%7D&nwSort=JVGroupEmail&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_STAFF = "https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22info@valorlists.com%22%7D%7D%5D%7D&nwSort=JVGroupEmail&nwPaging=%7B%22limit%22:15,%22offset%22:";
        private static string DIR_STUDENT = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22%5D%7D%7D%5D%7D&nwSort=$ex_Name:NameNameInverted&nwPaging=%7B%22limit%22:15,%22offset%22:"; // most of this URL is making sure it is just getting the students, and not the other things
        private static string DIR_FACULTY = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Faculty%22%5D%7D%7D%5D%7D&nwSort=$ex_Name:NameNameInverted&nwPaging=%7B%22limit%22:15,%22offset%22:"; // most of this URL is making sure it is just getting the faculty, and not the other things
        private static string DIR_STAFF = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Staff%22%5D%7D%7D%5D%7D&nwSort=$ex_Name:NameNameInverted&nwPaging=%7B%22limit%22:15,%22offset%22:"; // most of this URL is making sure it is just getting the staff, and not the other things

        public static string URL_END = "%7D";

        //used for token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "AppStable";
        private static string LIFECYCLE = "jake_v";



        public static List<DirectoryNW2.Record> getDirectoryRecords(string urlBase) // This origionally came from School Directory
        {
            DirectoryNW2.RootObject root = new DirectoryNW2.RootObject();
            List<DirectoryNW2.Record> list = new List<DirectoryNW2.Record>();

            //Get request to retrieve initial data from Nextworld
            //var client = new RestClient(DIR_BASE + "0" + URL_END);
            var client = new RestClient(urlBase + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<DirectoryNW2.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                //nextURL = DIR_BASE + nextOffset + URL_END;
                nextURL = urlBase + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new DirectoryNW2.RootObject();
                root = JsonConvert.DeserializeObject<DirectoryNW2.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }


        public static List<StudentModel> GetStudents()
        {
            List<StudentModel> list = new List<StudentModel>();

            //initial data request to Nextworld 
            List <DirectoryNW2.Record> root = getDirectoryRecords(DIR_STUDENT);

            for (int i = 0; i < root.Count; i++)
            {
                list.Add(convertStudentsToOldMoodel(root.ElementAt(i).appData));
            }
            
            return list;
        }

        private static StudentModel convertStudentsToOldMoodel(DirectoryNW2.AppData appData)
        {
            StudentModel temp = new StudentModel();
            if (appData.nwExternalId != null)
            {
                temp.EA7RecordsID = Regex.Match(appData.nwExternalId.ToString(), @"\d+").Value; // this returns just the numbers, as it is not EE_ every time ther is an external ID, like in Valor Prod

            }
            else
            {
                temp.EA7RecordsID = appData.nwId;
            }
            if (appData.Name.NameNamesBeforeKeyNames != null)
            {
                temp.FirstName = appData.Name.NameNamesBeforeKeyNames;
            }
            temp.Gender = appData.Gender;
            temp.GradeLevel = appData.GradeLevel.ToString();
            temp.GroupEmail = "classof" + appData.ClassOf + "@govalor.com"; // This is one I am going to have to fix
            temp.LastName = appData.Name.NameNameInverted.Substring(0, appData.Name.NameNameInverted.IndexOf(" "));
            temp.MiddleName = appData.Name.NameName;
            temp.NickName = appData.Nickname;
            temp.OrgUnitPath = "/Students/Class of " + appData.ClassOf; // Have to hard code this, as this data is not stored in nextworld. Maybe make this a command line arg
            temp.Password = (string)appData.nwExternalId; // this is another one I am going to have to fix
            temp.StudentEmail = appData.PrimaryEmail;
            temp.StudentID = appData.StudentID;
            temp.Title = appData.Name.NameName;
            return temp;
        }




        public static List<FacultyStaffModel> GetFacultyNoLoop()
        {

            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22faculty@valorchristian.com%22%7D%7D%5D%7D"); // This is different then the schoology one because it needs some other fields 

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                //temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;

        }

        public static List<FacultyStaffModel> GetFaculty()
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            List<DirectoryNW2.Record> root = new List<DirectoryNW2.Record>();

            //initial data request from Nextworld
            root = getDirectoryRecords(DIR_FACULTY);

            for (int i = 0; i < root.Count; i++)
            {
                list.Add(convertFacultyStaffToOldModel(root.ElementAt(i).appData));
            }

            

            return list;
        }


        public static List<FacultyStaffModel> GetStaff()
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            List<DirectoryNW2.Record> root = new List<DirectoryNW2.Record>();

            //initial data request from Nextworld
            root = getDirectoryRecords(DIR_STAFF);

            for (int i = 0; i < root.Count; i++)
            {
                list.Add(convertFacultyStaffToOldModel(root.ElementAt(i).appData));
            }



            return list;
        }

        private static FacultyStaffModel convertFacultyStaffToOldModel(DirectoryNW2.AppData appData)
        {
            FacultyStaffModel temp = new FacultyStaffModel();
            if (appData.nwExternalId != null)
            {
                temp.EA7RecordsID = Convert.ToInt32(Regex.Match(appData.nwExternalId.ToString(), @"\d+").Value); // this returns just the numbers, as it is not EE_ every time ther is an external ID, like in Valor Prod
                temp.FacultyID = appData.AlternateID; // 
            }
            temp.Email = appData.PrimaryEmail;
            temp.FirstName = appData.Name.NameNamesBeforeKeyNames;

            List<string> ContactRoleList = new List<string>();

            if (appData.ContactRole.GetType().Equals(new List<string>()))
            {
                ContactRoleList = (List<string>)appData.ContactRole;
            }
            else
            {
                ContactRoleList.Add(appData.ContactRole.ToString());
            }



            if (ContactRoleList.IndexOf("Faculty") < 0){ // This person is ONLY a staff, not a faculty
                temp.GroupEmail = "info@valorlists.com,allhands@myvalor.com,classifieds@valorlists.com"; // hard coded because it is not in nw app
            }
            else
            {
                temp.GroupEmail = "faculty@valorchristian.com,info@valorlists.com,allhands@myvalor.com,classifieds@valorlists.com"; // Need to hard code this because it is not included in the NW application

            }
            temp.LastName = appData.Name.NameKeyNames;
            temp.OrgUnitPath = "/Faculty-Staff"; // Have to hard code this because the info isn't in NW
            temp.Password = "2017Govalor"; // Hard coded because the data is not in NW
            temp.nwID = appData.nwId;
            return temp;
        }

        private static FacultyStaffNW.RootObject getRootFaculty(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<FacultyStaffNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        public static List<FacultyStaffModel> GetStaffNoLoop()
        {
            List<FacultyStaffModel> temp = new List<FacultyStaffModel>();

            FacultyStaffNW.RootObject rootFacultyStaff = new FacultyStaffNW.RootObject();

            rootFacultyStaff = getRootFaculty("https://master-api1.nextworld.net/v2/JVAdminFacultyStaff?nwFilter=%7B%22$and%22:%5B%7B%22JVGroupEmail%22:%7B%22$like%22:%22info@valorlists.com%22%7D%7D%5D%7D"); // This is different than the schoology one because it needs a couple different paramaters 

            for (int i = 0; i < rootFacultyStaff.Data.records.Count; i++)
            {
                //temp.Add(convertFacultyStaffToOldModel(rootFacultyStaff.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        // Before using the new Directory Table in the API Call
        //public static List<FacultyStaffModel> GetStaff()
        //{
        //    List<FacultyStaffModel> list = new List<FacultyStaffModel>();

        //    FacultyStaffNW.RootObject root = new FacultyStaffNW.RootObject();

        //    //initial data request from Nextworld
        //    root = getRootFaculty(URL_BASE_STAFF + "0" + URL_END);

        //    for (int i = 0; i < root.Data.records.Count; i++)
        //    {
        //        //list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
        //    }

        //    //Loop through the student api and concatenate all the students for later use
        //    int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
        //    String nextURL = "";
        //    String nextOffset = "";

        //    while (endTest < root.Data.pageData.total)
        //    {
        //        //find next url
        //        nextOffset = endTest.ToString();

        //        nextURL = URL_BASE_STAFF + nextOffset + URL_END;

        //        //get the next set of data from Nextworld
        //        root = new FacultyStaffNW.RootObject();
        //        root = getRootFaculty(nextURL);

        //        for (int i = 0; i < root.Data.records.Count; i++)
        //        {
        //            //list.Add(convertFacultyStaffToOldModel(root.Data.records.ElementAt(i).appData));
        //        }

        //        //update the end test
        //        endTest = root.Data.pageData.offset + root.Data.pageData.limit;
        //    }

        //    return list;
        //}

        //This is an old method before we started using the actual direcory table without any looping as well
        //public static  List<StudentModel> GetStudentsNoLoop()
        //{
        //    List<StudentModel> temp = new List<StudentModel>();

        //    StudentNW.RootObject rootStudents = getRootStudents("https://master-api1.nextworld.net/v2/JVAdminGetStudents");

        //    for (int i = 0; i < rootStudents.Data.records.Count; i++)
        //    {
        //        temp.Add(convertStudentsToOldMoodel(rootStudents.Data.records.ElementAt(i).appData));
        //    }

        //    return temp;
        //}

        // This is the method that was used before using the actual directory table
        //public static List<StudentModel> GetStudents()
        //{
        //    List<StudentModel> list = new List<StudentModel>();

        //    //initial data request to Nextworld 
        //    StudentNW.RootObject root = getRootStudents(URL_BASE_STUDENT + "0" + URL_END);

        //    for (int i = 0; i < root.Data.records.Count; i++)
        //    {
        //        list.Add(convertStudentsToOldMoodel(root.Data.records.ElementAt(i).appData));
        //    }

        //    //Loop through the student api and concatenate all the students for later use
        //    int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
        //    String nextURL = "";
        //    String nextOffset = "";

        //    while (endTest < root.Data.pageData.total)
        //    {
        //        //find next url
        //        nextOffset = endTest.ToString();

        //        nextURL = URL_BASE_STUDENT + nextOffset + URL_END;

        //        //get the next set of data from Nextworld
        //        root = new StudentNW.RootObject();
        //        root = getRootStudents(nextURL);

        //        for (int i = 0; i < root.Data.records.Count; i++)
        //        {
        //            list.Add(convertStudentsToOldMoodel(root.Data.records.ElementAt(i).appData));
        //        }

        //        //update the end test
        //        endTest = root.Data.pageData.offset + root.Data.pageData.limit;
        //    }

        //    return list;
        //}


        // This is the old method before using the new directory table
        //private static StudentModel convertStudentsToOldMoodel(StudentNW.AppData appData)
        //{
        //    StudentModel temp = new StudentModel();
        //    temp.EA7RecordsID = appData.JVEA7RecordsID;
        //    temp.FirstName = appData.JVFirstName;
        //    temp.Gender = appData.JVGender;
        //    temp.GradeLevel = appData.JVGradeLevel;
        //    temp.GroupEmail = appData.JVGroupEmail;
        //    temp.LastName = appData.JVLastName;
        //    temp.MiddleName = appData.JVMiddleName;
        //    temp.NickName = appData.JVNickName;
        //    temp.OrgUnitPath = appData.JVOrgUnitPath;
        //    temp.Password = appData.JVPassword;
        //    temp.StudentEmail = appData.JVEmail;
        //    temp.StudentID = appData.JVUserDefinedID;
        //    temp.Title = appData.JVTitle;
        //    return temp;
        //}

        // This was used with the old API and Table
        //private static StudentNW.RootObject getRootStudents(string url)
        //{
        //    var client = new RestClient(url);
        //    var request = new RestRequest(Method.GET);

        //    //using a token bearer to authenticate request
        //    ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
        //    client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        //    client.AddDefaultHeader("Content-Type", "application/json");

        //    StudentNW.RootObject root = new StudentNW.RootObject();

        //    var response = client.Execute(request);
        //    // Writes to the console the responce we got from the HTTP request
        //    //Console.WriteLine(response.Content);

        //    root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
        //    //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

        //    return root;
        //}
    }
}
