﻿using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class DonationReceiptTransaction
    {
        //public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");

        //Token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("integration.valor", "11Govalor01!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "ValorIntegration";
        private static string LIFECYCLE = "valor";

        public static NWDonationReceiptsTransaction.RootObject genDonationReceiptTransaction(string nwid)
        {

            NWDonationReceiptsTransaction.RootObject temp = new NWDonationReceiptsTransaction.RootObject();


            //var client = new RestClient("https://master-api1.nextworld.net/v2/DonationReceipts/" + nwid);
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/DonationReceiptsTransaction/" + nwid);

        

            var request = new RestRequest(Method.GET);
            //client.Authenticator = auth;


            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            temp = JsonConvert.DeserializeObject<NWDonationReceiptsTransaction.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return temp;
        }
    }
}
