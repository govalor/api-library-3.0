﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    /*
     *  EligibilityDirectory is the class that provides production API access for Schoology Reports - Eligibility.
     *  
     *  This class pulls data from the CourseSections table (SECTION_BASE), Courses table (COURSES_BASE), StudentGrades table (GRADES_BASE),
     *          and the Directory table, filtered for either students (STUDENT_BASE) or teachers (TEACHER_BASE) in Nextworld.
     *  
     *  To view the simulation API class, see SchoologyEligibility under the Programs folder.
     */
    public class EligibilityDirectory
    {
        //API URLs
        private string SECTION_BASE = "https://master-api1.nextworld.net/v2/CourseSections?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string COURSE_BASE = "https://master-api1.nextworld.net/v2/Courses?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string GRADES_BASE = "https://master-api1.nextworld.net/v2/StudentGrades?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string TEACHER_BASE = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Faculty%22%5D%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string STUDENT_BASE = "https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactType%22:%22Person%22%7D,%7B%22ContactRoleGrouping%22:%7B%22$multi%22:%5B%22STU%22,%22PAR%22,%22EMP%22,%22FRMSTU%22,%22INCSTU%22%5D%7D%7D,%7B%22ContactRole%22:%7B%22$multi%22:%5B%22Student%22%5D%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_END = "%7D";

        //Token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "base";

        public EligibilityDirectory() { }

        /*
         *  getStudentGrade() - This method retrieves all of the student grades records from the StudentGrades table in Nextworld.
         *                          *Note: Implements api url paging by offset.
         */ 
        private List<StudentGrades.Record> getStudentGrades()
        {
            StudentGrades.RootObject root = new StudentGrades.RootObject();
            List<StudentGrades.Record> list = new List<StudentGrades.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(GRADES_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<StudentGrades.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next offset and url
                nextOffset = endTest.ToString();

                nextURL = GRADES_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new StudentGrades.RootObject();
                root = JsonConvert.DeserializeObject<StudentGrades.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }

        /*
         *  getCourseSections() - This method retrieves all of the course sections from the CourseSections table in Nextworld.
         *                           *Note: Implements api url paging by offset.
         */
        private List<CourseSection.Record> getCourseSections()
        {
            CourseSection.RootObject root = new CourseSection.RootObject();
            List<CourseSection.Record> list = new List<CourseSection.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(SECTION_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<CourseSection.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next offset and url
                nextOffset = endTest.ToString();

                nextURL = SECTION_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new CourseSection.RootObject();
                root = JsonConvert.DeserializeObject<CourseSection.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }

        /*
         *  getCourses() - This method retrieves all of the corses offered from the Courses table in Nextworld.
         *                    *Note: Implements api url paging by offset.
         */
        private List<Courses.Record> getCourses()
        {
            Courses.RootObject root = new Courses.RootObject();
            List<Courses.Record> list = new List<Courses.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(COURSE_BASE + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<Courses.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next offset and url
                nextOffset = endTest.ToString();

                nextURL = COURSE_BASE + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new Courses.RootObject();
                root = JsonConvert.DeserializeObject<Courses.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }

        /*
         *  getDjrectoryRecords() - This method retrieves either the students or the teachers from the Directory table in Nextworld.
         *                            *Note: Implements api url paging by offset.
         *                            
         *  @param urlBase - This indicates whether the desired data is students or teachers, and will either be TEACHER_BASE or STUDENT_BASE.
         */
        private List<DirectoryNW2.Record> getDirectoryRecords(string urlBase)
        {
            DirectoryNW2.RootObject root = new DirectoryNW2.RootObject();
            List<DirectoryNW2.Record> list = new List<DirectoryNW2.Record>();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(urlBase + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<DirectoryNW2.RootObject>(response.Content);

            list = list.Concat(root.Data.records).ToList();

            //Loop through the pages to obtain all data
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = urlBase + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new DirectoryNW2.RootObject();
                root = JsonConvert.DeserializeObject<DirectoryNW2.RootObject>(response1.Content);

                list = list.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }
            Console.WriteLine(list.Count());
            return list;
        }

        /*
         *  findStudent() - This method finds a particular student from the list of all students retrieved from Nextworld.
         *  
         *  @param nwID - This is the Nextworld ID of the student to be found.
         *  
         *  @param studentList - This is the list of all student records from Nextworld.
         */ 
        private DirectoryNW2.Record findStudent(string nwID, List<DirectoryNW2.Record> studentList)
        {
            DirectoryNW2.Record student = new DirectoryNW2.Record();

            //studentList = studentList.Where(x => x.appData.nwId == nwID).ToList();

            foreach (DirectoryNW2.Record s1 in studentList)
            {
                if (s1.appData.nwId == nwID)
                {
                    student = s1;
                    break;
                }
            }


            return student;
        }

        /*
        *  findSection() - This method finds a particular section of a course offered from the list of all sections retrieved from Nextworld.
        *  
        *  @param nwID - This is the Nextworld ID of the section to be found.
        *  
        *  @param sectionList - This is the list of all sections from Nextworld.
        */
        private CourseSection.Record findSection(string nwID, List<CourseSection.Record> sectionList)
        {
            CourseSection.Record section = new CourseSection.Record();

            //sectionList = sectionList.Where(x => x.appData.nwId == nwID).ToList();

            foreach (CourseSection.Record s in sectionList)
            {
                if (s.appData.nwId == nwID)
                {
                    section = s;
                    break;
                }
            }

            //section = sectionList.ElementAt(0);

            return section;
        }

        /*
        *  findTeacher() - This method finds a particular teacher from the list of all teachers retrieved from Nextworld.
        *  
        *  @param nwID - This is the Nextworld ID of the teacher to be found.
        *  
        *  @param teacherList - This is the list of all teacher records from Nextworld.
        */
        private DirectoryNW2.Record findTeacher(string nwID, List<DirectoryNW2.Record> teacherList)
        //private string findTeacher(string nwID, List<DirectoryNW2.Record> teacherList)
        {
            DirectoryNW2.Record teacher = new DirectoryNW2.Record();

            //teacher = teacherList.Where(x => x.appData.nwId == nwID).ElementAt(0);

            foreach (DirectoryNW2.Record teach in teacherList)
            {
                if (teach.appData.nwId == nwID)
                {
                    teacher = teach;
                    break;
                }
            }

            return teacher;
            //return teacher.appData.Name.NameName;
        }

        /*
        *  findCourse() - This method finds a particular course from the list of all courses retrieved from Nextworld.
        *  
        *  @param nwID - This is the Nextworld ID of the course to be found.
        *  
        *  @param courseList - This is the list of all the courses offered that were retrieved from Nextworld.
        */
        private Courses.Record findCourse(string nwID, List<Courses.Record> courseList)
        {
            Courses.Record course = new Courses.Record();

            //return courseList.Where(x => x.appData.nwId == nwID).ElementAt(0); //check

            foreach (Courses.Record c1 in courseList)
            {
                if (c1.appData.nwId == nwID)
                {
                    course = c1;
                    break;
                }
            }
            return course;
        }

        /*
        *  makeReportModel() - This method uses all the data retrieved to create a single StudentGradesModel that represents a single class taken by a student.
        *  
        *  @param student - The Directory record of a particular student from Nextworld.
        *  
        *  @param teacher - The name of the person that teaches this particular class.
        *  
        *  @param course - The Course record of the particular class this student is taking.
        *  
        *  @param section - The particular section of the course being taken.
        *  
        *  @param gradeNumber - The grade (in decimal form) that the student currently has in this class. (Ex: 0.85)
        */
        //private StudentGradesModel makeReportModel(DirectoryNW2.Record student, DirectoryNW2.Record teacher, Courses.Record course, CourseSection.Record section, double gradeNumer)
        private StudentGradesModel makeReportModel(DirectoryNW2.Record student, string teacher, Courses.Record course, string section, double gradeNumer)
        {
            StudentGradesModel report = new StudentGradesModel()
            {
                EA7RecordsID = 0, //check for what we want to do with this
                StudentID = student.appData.StudentID,
                FirstName = student.appData.Name.NameNamesBeforeKeyNames,
                LastName = student.appData.Name.NameKeyNames,
                GradeLevel = (int)student.appData.GradeLevel,
                SectionCode = " ", //check about this - not sure where this data is in Nextworld
                //CourseSection = section.appData.CourseCodeSection,
                CourseSection = section,
                //Faculty = teacher.appData.Name.NameName,
                Faculty = teacher,
                Course = course.appData.CourseCode,
                ClassName = course.appData.CourseName,
                Grade = (gradeNumer * 100),
                //GroupTitle = " " //eventually delete, after simulation apis have been removed
            };

            return report;
        }

        /*
         *  GetAllGrades() - This method combines all of the above to retrieve the necessary data from Nextworld and create grade reports for all
         *                      the students in the Directory table.
         */ 
        public List<StudentGradesModel> GetAllGrades()
        {
            List<StudentGradesModel> allGrades = new List<StudentGradesModel>();

            //Retrieve data from Nextworld
            List<StudentGrades.Record> gradeReports = getStudentGrades();
            List<CourseSection.Record> courseSections = getCourseSections();
            List<Courses.Record> courses = getCourses();
            List<DirectoryNW2.Record> students = getDirectoryRecords(STUDENT_BASE);
            List<DirectoryNW2.Record> teachers = getDirectoryRecords(TEACHER_BASE);

            CourseSection.Record section = new CourseSection.Record();
            StudentGradesModel report = new StudentGradesModel();
            DirectoryNW2.Record teacher = new DirectoryNW2.Record();
            DirectoryNW2.Record student = new DirectoryNW2.Record();
            Courses.Record course = new Courses.Record();

            foreach (StudentGrades.Record grade in gradeReports)
            {
                section = new CourseSection.Record();
                report = new StudentGradesModel();
                teacher = new DirectoryNW2.Record();
                student = new DirectoryNW2.Record();
                course = new Courses.Record();

                if (grade.appData.TransferCourseName != "Marine Biology") //for testing purposes, delete later
                {

                    string t1 = "";
                    string sectionName = "";

                    student = findStudent(grade.appData.Student, students);

                    if (student.appData != null)
                    {
                        course = findCourse(grade.appData.Course, courses);

                        if (string.IsNullOrEmpty(grade.appData.CourseSection))
                        {
                            //data fillers
                            sectionName = "NA";
                            t1 = "Unknown";
                        }
                        else
                        {
                            section = findSection(grade.appData.CourseSection, courseSections);
                            sectionName = section.appData.CourseCodeSection; //added - hopefully delete?

                            teacher = findTeacher(section.appData.Teacher, teachers);

                            if (teacher.appData == null)
                            {
                                t1 = "Unknown";
                            }
                            else
                            {
                                t1 = teacher.appData.Name.NameName;
                            }
                        }


                        //StudentGradesModel report = makeReportModel(student, teacher, course, section, grade.appData.GradeNumber);

                        report = makeReportModel(student, t1, course, sectionName, grade.appData.GradeNumber);

                        allGrades.Add(report);
                    }
                }
            }

            return allGrades;
        }
    }
}
