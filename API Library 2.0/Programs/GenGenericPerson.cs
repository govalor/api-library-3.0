﻿using API_Library_2._0.Translation_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public static class GenGenericPerson
    {
        public static GenericPerson getGenericPerson(string nwid)
        {
            GenericPerson genericPerson = new GenericPerson();

            NWDirectory.RootObject nwdir = Directory.getDirectoryRecord(nwid);

            NWDirectory.AppData appData = nwdir.Data.records.ElementAt(0).appData;

            genericPerson.NameName = appData.Name.NameName;
            if (genericPerson.NameName != null && genericPerson.NameName != "" && genericPerson.NameName.IndexOf(" ") > 0)
            {
                //genericPerson.FirstName = appData.Name.NameName.Substring(0, appData.Name.NameName.IndexOf(" "));
                //genericPerson.LastName = appData.Name.NameName.Substring(appData.Name.NameName.IndexOf(" "));

                genericPerson.FirstName = appData.Name.NameNamesBeforeKeyNames;
                genericPerson.LastName = appData.Name.NameKeyNames;

            }
            // genericPerson.PrimaryEmail = appData.Emails.;
            genericPerson.Gender = appData.Gender;
            genericPerson.Anonymous = appData.Anonymous;
            genericPerson.TransferStudent = appData.TransferStudent;
            genericPerson.EnrollmentStatus = appData.EnrollmentStatus;
            genericPerson.GPAElegibilityStatus = appData.GPAEligibilityStatus;
            genericPerson.SocialSecurityNumber = appData.SocialSecurityNumber;
            genericPerson.VisaNumber = appData.VisaNumber;
            genericPerson.PassportNumber = appData.PassportNumber;
            genericPerson.PhoneNumber = GetPrimaryPhoneNumber(appData.Phones);
            genericPerson.PrimaryEmail = GetPrimaryEmail(appData.Emails);

            if (appData.PrimaryAddress != null)
            {
                genericPerson.Address = appData.PrimaryAddress.AddressFull;
            }
            return genericPerson;
        }

        public static string GetPrimaryPhoneNumber(List<NWDirectory.Phone> listPhones)
        {
            string primaryPhone = "";

            foreach (NWDirectory.Phone phone in listPhones)
            {
                if (phone.Primary)
                {
                    primaryPhone = phone.PhoneNumber;
                }
            }
            return primaryPhone;
        }

        public static string GetPrimaryEmail(List<NWDirectory.Email> listEmails)
        {
            string primaryEmail = "";

            foreach (NWDirectory.Email email in listEmails)
            {
                if (email.Primary)
                {
                    primaryEmail = email.EmailAddress;
                }
            }
            return primaryEmail;
        }
    }



}
