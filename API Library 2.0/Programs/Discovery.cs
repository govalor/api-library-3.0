﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Programs;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Discovery
    {
        //public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("integration.valor", "11Govalor01!");
        //public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("kurt.valor", "33Govalor03!");

        //Token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("integration.valor", "11Govalor01!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "ValorIntegration";
        private static string LIFECYCLE = "valor";


        public static List<SolicitorInfo2> GetSolicitorDataForFund(string sid, string fid) // these are currently nwids, will need to change to external ID
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();

            int saveSID = Int32.Parse(sid);
            int savefID = Int32.Parse(fid);

            if (sid.Count() < 32)
            {
                // this means that it probably isn't the nwid, and is in fact an external ID. we have to look up and find the nwID based of this external ID

                NWDirectory.RootObject sidResult= Directory.getDirectoryRecordByExternalID("EE_" + sid);
                sid = sidResult.Data.records.ElementAt(0).appData.nwId;
            }

            if (fid.Count() < 32)
            {
                // this means that it probably isn't the nwid, and is in fact an external ID. we have to look up and find the nwID based of this external ID

                NWFund.RootObject fund = Fund.genDonationReceiptTransactionByExternalID("F_" + fid);
                fid = fund.Data.records.ElementAt(0).appData.nwId;
            }

            //var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22"+sid+"%22%7D%7D,%7B%22FundID%22:%7B%22$like%22:%22"+fid+"%22%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:0%7D");
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22" + sid + "%22%7D%7D,%7B%22FundID%22:%7B%22$like%22:%22" + fid + "%22%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:0%7D");

            //client.Authenticator = auth;

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content); // This root object is from the DonationFundDistribution. It mostly contains things that are IDs to other things. For this to be usefull, we also have to do quite a few more API Calls. 

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(saveSID, savefID, root.Data.records.ElementAt(i)));
            }

            temp = temp.Distinct().OrderBy(d => d.CreatedOnDate).ToList();
            return temp;
        }

        private static SolicitorInfo2 convertToSolicitorInfo2(int sid, int fid, NWSolicitorInfo2.Record record)
        {

            GenericPerson donor = GenGenericPerson.getGenericPerson(record.appData.Donor); // generic person is an easier to use directory "final model" per se. This is generating that generic person based off the donor nwid

            NWDirectory.RootObject donorDirectory = Directory.getDirectoryRecord(record.appData.Donor); // This is just getting the full directory recored basesed of the Donor nwid

            //NWStudentActivitiesHeader.RootObject activity = StudentActivities.genStudentActivites(record.appData.FundID); // this gets the activity based of the fund. This is needed for getting the name of the activity and experinece description

            //GenericPerson leader = GenGenericPerson.getGenericPerson(activity.Data.records.ElementAt(0).appData.ActivityLeader);

            //NWActivityDefinition.RootObject activityDefinitoin = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);

            // Getting the infromation from the header
            NWDonationReceiptsTransaction.RootObject donationReceiptTransaction = DonationReceiptTransaction.genDonationReceiptTransaction(record.appData.nwHeaderId); // this is getting the header detail
            NWDonationReceiptsTransaction.AppData donationReceptData = donationReceiptTransaction.Data.records.ElementAt(0).appData;

            SolicitorInfo2 temp = new SolicitorInfo2();

            // LineItemID missing


            temp.Address = donor.Address;
            temp.AmountAnonymous = Convert.ToInt32(0); 
            temp.CityStateZip = donorDirectory.Data.records.ElementAt(0).appData.PrimaryAddress.AddressCity + ", " + donorDirectory.Data.records.ElementAt(0).appData.PrimaryAddress.AddressState + " " + donorDirectory.Data.records.ElementAt(0).appData.PrimaryAddress.AddressPostalCode; // Missing zipcode
            temp.Contributor = donor.NameName;
            temp.ContributorNotes = "Have Fun!!!"; // record.appData.DonationMemo;
            temp.CreatedOnDate = DateTime.Parse(donationReceptData.TransactionDate);
            temp.DonorAnonymous = Convert.ToInt32(0);
            temp.Email = donor.PrimaryEmail;
            temp.ExperienceDescription = "Debugging"; // activityDefinitoin.Data.records.ElementAt(0).appData.ActivityName; 
            temp.FirstName = donor.FirstName;
            temp.FundId = fid; //record.appData.FundID;
            temp.GiftSolicitorId = sid; //record.appData.Solicitor;
            
            temp.GiftTotal = Convert.ToDecimal(record.appData.DonationAmount.CurrencyValue.ToString().Insert(record.appData.DonationAmount.CurrencyValue.ToString().Length - record.appData.DonationAmount.CurrencyDecimals, "."));
            temp.LastName = donor.LastName;
            temp.LineItemId = Convert.ToInt32(donationReceptData.nwExternalId);
            temp.Phone = donor.PhoneNumber;
            temp.TotalFunds = 0.0M; // (decimal)record.appData.DonationAmount.CurrencyValue;

            return temp;
        }



        public static List<SolicitorFunds> GetSolicitorFunds(string sid) // the nwid is going to be replaced by the actual ID in valor prod. The URL will also have to change to match
        {
            List<SolicitorFunds> temp = new List<SolicitorFunds>();

            if (sid.Count() < 32)
            {
                // this means that it probably isn't the nwid, and is in fact an external ID. we have to look up and find the nwID based of this external ID

                NWDirectory.RootObject sidResult = Directory.getDirectoryRecordByExternalID("EE_" + sid);
                sid = sidResult.Data.records.ElementAt(0).appData.nwId;
            }
            //var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22"+sid+"%22%7D%7D%5D%7D");
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22" + sid + "%22%7D%7D%5D%7D");

            //var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/DonationFundDistribution?nwFilter={'$and':[{'Solicitor':{'$like':'" + sid + "'}}]}");
            // client = new RestClient("https://api-o1p.prod.nextworld.net/v2/DonationFundDistribution?nwFilter={'$and':[{'Solicitor':{'$like':'d8514d9c-6f42-11e8-b0f8-020d0599343e'}}]}");
            //client.Authenticator = auth;

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorFunds.RootObject root = JsonConvert.DeserializeObject<NWSolicitorFunds.RootObject>(response.Content);

            //List<NWFund.AppData> listFunds = new List<NWFund.AppData>();

            //for (int i = 0; i < root.Data.records.Count; i++)
            //{
            //    NWFund.RootObject fund = Fund.genFundbyNWID(root.Data.records.ElementAt(i).appData.FundID);
            //    listFunds.Add(fund.Data.records.ElementAt(0).appData);
 
            //}
            //listFunds = listFunds.Where(x => x.FundDescription.Contains("Discovery")).ToList();
            //for (int i = 0; i < listFunds.Count; i++)
            //{
            //    temp.Add(convertToSolicitorFunds(listFunds.ElementAt(0));
            //}

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                NWFund.RootObject fund = Fund.genFundbyNWID(root.Data.records.ElementAt(i).appData.FundID);

                if (fund.Data.records.ElementAt(0).appData.FundDescription.Contains("Discovery"))
                {
                    temp.Add(convertToSolicitorFunds(root.Data.records.ElementAt(i).appData));
                }
            }

            temp = temp.Distinct().OrderBy(fund => fund.FundId).ToList();

            return temp;


        }


        private static SolicitorFunds convertToSolicitorFunds(NWSolicitorFunds.AppData appData)
        {

            //NWStudentActivitiesHeader.RootObject activity = StudentActivities.genStudentActivites(appData.FundID);
            //NWActivityDefinition.RootObject activityDef = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);

            SolicitorFunds temp = new SolicitorFunds();

            //temp.Description = activityDef.Data.records.ElementAt(0).appData.ActivityName;

            NWFund.RootObject fund = Fund.genFundbyNWID(appData.FundID);
            temp.FundId = Int32.Parse(fund.Data.records.ElementAt(0).appData.nwExternalId.ToString().Substring(2)); // Need the value of the FundID and not the NWID
            temp.Description = fund.Data.records.ElementAt(0).appData.FundName;

            NWDirectory.RootObject solicitor = Directory.getDirectoryRecord(appData.Solicitor);
            temp.SolicitorId = Int32.Parse(solicitor.Data.records.ElementAt(0).appData.nwExternalId.ToString().Substring(3)); // Need the value of the SolicitorID and not the NWID

            return temp;
        }

        public static List<LeaderFunds> GetLeaderStatus(string nwid)
        {
            // example nwid 068a96a8-4420-11e8-a2f9-0657f54c5f30
            // The .Count is 36 
            List<LeaderFunds> temp = new List<LeaderFunds>();
            NWStudentActivitiesHeader.RootObject activity;


            if (nwid.Count() < 36)
            {
                // this means that the nwID probably isn't an nwID and that it is actually an External ID. This means that we need to do a lookup and see get the nwID based off the externalID
                NWDirectory.RootObject activityLeader = Directory.getDirectoryRecordByExternalID("EE_" + nwid);
                nwid = activityLeader.Data.records.ElementAt(0).appData.nwId;
                // now we are going to recall the method with the nextworld ID. This is a little bit redundent but better safe then sorry

                activity = StudentActivities.genStudentActivityByLeaderID(activityLeader.Data.records.ElementAt(0).appData.nwId);
            }
            else
            {
                activity = StudentActivities.genStudentActivityByLeaderID(nwid);

            }
            for (int i =0; i < activity.Data.records.Count; i++)
            {
                temp.Add(convertToLeaderFunds(activity.Data.records.ElementAt(i), nwid));
            }

            temp = temp.Distinct().ToList();

            return temp;
        }

        private static LeaderFunds convertToLeaderFunds(NWStudentActivitiesHeader.Record record, string leaderId)
        {
            LeaderFunds temp = new LeaderFunds();

            NWActivityDefinition.RootObject actDef = ActivityDefinition.genActivityDefinition(record.appData.Activity);

            //temp.ExperienceDescription = actDef.Data.records.ElementAt(0).appData.ActivityName;
            //temp.FundId = record.appData.AssociatedFund;
            //temp.LeaderId = leaderId;

            NWFund.RootObject fund = Fund.genFundbyNWID(record.appData.AssociatedFund);
            temp.FundId = Int32.Parse(fund.Data.records.ElementAt(0).appData.nwExternalId.ToString().Substring(2)); // Need the value of the FundID and not the NWID
            temp.ExperienceDescription = fund.Data.records.ElementAt(0).appData.FundName;

            NWDirectory.RootObject leader = Directory.getDirectoryRecord(leaderId);
            temp.LeaderId = Int32.Parse(leader.Data.records.ElementAt(0).appData.nwExternalId.ToString().Substring(3)); // Need the value of the SolicitorID and not the NWID

            return temp;
        }

        public static List<AdminFundDetail> GetAdminDetail(string start, string end)
        {
            List<AdminFundDetail> temp = new List<AdminFundDetail>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution/");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);
            
            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToAdminFundDetail(root.Data.records.ElementAt(i).appData));
            }
            return temp;
        }

        private static AdminFundDetail convertToAdminFundDetail(NWSolicitorInfo2.AppData appData)
        {
            AdminFundDetail temp = new AdminFundDetail();

            // Getting the infromation from the header
            NWDonationReceiptsTransaction.RootObject donationReceiptTransaction = DonationReceiptTransaction.genDonationReceiptTransaction(appData.nwHeaderId); // this is getting the header detail
            NWDonationReceiptsTransaction.AppData  donationReceptData = donationReceiptTransaction.Data.records.ElementAt(0).appData;

            // fund info
            NWStudentActivitiesHeader.RootObject activity = null;
            NWActivityDefinition.RootObject activityDef = null;
            if (appData.FundID != null)
            {
                activity = StudentActivities.genStudentActivites(appData.FundID);
                if (activity.Data.records.Count > 0)
                {
                    activityDef = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);
                }
            }


            NWDirectory.RootObject donor = Directory.getDirectoryRecord(appData.Donor);
            NWDirectory.AppData donorData = donor.Data.records.ElementAt(0).appData;
            GenericPerson donorPerson = GenGenericPerson.getGenericPerson(appData.Donor);
            NWDonationFundsDetail.RootObject donationDetail = DonationFundDetail.getDonationFundDetail(appData.nwId);
            // getting the solicitor
            NWDirectory.AppData solicitorData = null;
            
            if (appData.Solicitor != null)
            {
                NWDirectory.RootObject solicitor = Directory.getDirectoryRecord(appData.Solicitor);

                if (solicitor != null &&solicitor.Data.records.ElementAt(0) != null)
                {
                    solicitorData = solicitor.Data.records.ElementAt(0).appData;

                }

            }

            NWDonationFundsDetail.AppData donationData = donationDetail.Data.records.ElementAt(0).appData;

                
            temp.CheckDate = ""+donationReceptData.CheckDate;
            temp.CheckNumber = "" + donationReceptData.CheckNumber;
            if (donorData.PrimaryAddress != null)
            {
                if (donorData.PrimaryAddress.AddressCity != null)
                {
                    temp.CityStateZip = temp.CityStateZip + donorData.PrimaryAddress.AddressCity;
                    temp.City = donorData.PrimaryAddress.AddressCity;
                }

                if (donorData.PrimaryAddress.AddressState != null)
                {
                    temp.CityStateZip = temp.CityStateZip + " " +donorData.PrimaryAddress.AddressState;
                    temp.State = donorData.PrimaryAddress.AddressState;
                }
                if (donorData.PrimaryAddress.AddressPostalCode != null)
                {
                    temp.CityStateZip = temp.CityStateZip + " " + donorData.PrimaryAddress.AddressPostalCode;
                    temp.ZipCode = donorData.PrimaryAddress.AddressPostalCode;
                }
                if (donorData.PrimaryAddress.AddressFull != null)
                {
                    temp.Address = donorData.PrimaryAddress.AddressFull;
                }
            }
            temp.Contributor = donorData.Name.NameName;
            temp.ContributorNotes = appData.DonationMemo;
            temp.CreatedOnDate = "" + appData.nwCreatedDate;
            if (activityDef != null)
            {
                temp.ExperienceDescription = activityDef.Data.records.ElementAt(0).appData.ActivityName;
            }
            temp.FirstName = donorPerson.FirstName; 
            temp.FundId = donationData.FundID;
            temp.FundSplitAmount = donationData.DonationAmount.CurrencyValue;
            temp.GiftDate = donationReceptData.PostingDate;
            temp.GiftId = appData.nwId;
            temp.GiftPaymentType = donationReceptData.PaymentMethod;
            temp.GiftReference = "Team " + temp.ExperienceDescription;
            temp.GiftReferenceDate = donationReceptData.PostingDate;
            if (solicitorData != null && solicitorData.Name != null)
            {
                temp.GiftSolicitorFirstName = solicitorData.Name.NameNamesBeforeKeyNames;
                temp.GiftSolicitorLastName = solicitorData.Name.NameKeyNames; // solicitor from detail view
                    
            }
            temp.GiftSolicitorId = "" + donationData.Solicitor;
            temp.GiftSubType = donationReceptData.GiftInKindAccount;
            temp.GiftType = donationReceptData.GiftClassification; // might be backwards with the one above
            temp.LastName = donorPerson.LastName;
            temp.LineItemId = appData.nwId;
            temp.Name = donorPerson.NameName;
            temp.Phone = donorPerson.PhoneNumber;
            temp.PrimaryEmail = donorPerson.PrimaryEmail;
            temp.SecondaryEmail = donorPerson.PrimaryEmail;
            

            return temp;
        }
    }


}
