﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    /*
     *  Attendance is the class that provides simulation API access for Front Desk Attendance Reports.
     *  
     *  This class pulls data from the CMFacultyClassAttendanceTaken table (URL_BASE_GRADES) in Nextworld.
     *  
     *  There is currently no production API class.
     */
    public class Attendance
    {
        //API URLs
        private string URL_BASE_GRADES = "https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_END = "%7D";

        //Token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "cheyanne";

        public Attendance(){}

        /*
         *  getStudentGrades() - Retrieves the data from the CMFacultyClassAttendanceTaken table in Nextworld.
         *  
         *  **Note: Does not implement API paging by offset.
         */
        public List<GetFacultyClassAttendanceTaken_Result> getStudentGrades()
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();
            
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toAttendanceCourses(getRoot);

            return list;
        }

        /*
         *  getStudentGradesLoop() -  Retrieves the data from the CMFacultyClassAttendanceTaken table in Nextworld.
         *  
         *  **Note: Implements API paging by offset.
         */
        public List<GetFacultyClassAttendanceTaken_Result> getStudentGradesLoop()
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();
            
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient(URL_BASE_GRADES + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);
            
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);
 
            list = toAttendanceCourses(getRoot);

            //Loop through the grades api and concatenate all the grades for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_GRADES + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new AttendanceCourse.RootObject();
                getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response1.Content);

                list = list.Concat(toAttendanceCourses(getRoot)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        /*
         *  toAttendanceCourses() - This method translates the data from the model that corresponds with the JSON payload from the simulation 
         *                              application on Nextworld into the model that is used in the Front Desk Attendance custom program.
         *                              
         *   @param root - The root object that contains all the attendance records from Nextworld.
         */
        public List<GetFacultyClassAttendanceTaken_Result> toAttendanceCourses(AttendanceCourse.RootObject root)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            List<AttendanceCourse.Record> courses = root.Data.records;

            GetFacultyClassAttendanceTaken_Result course;
            AttendanceCourse.Record record;

            for (int i = 0; i < courses.Count(); i++)
            {
                course = new GetFacultyClassAttendanceTaken_Result();
                record = courses.ElementAt(i);

                course.BlockColor = record.appData.CMBlockColor;
                course.CourseCode = record.appData.CMCourseCode;
                course.CourseName = record.appData.CMCourse;
                course.FacultyEmail = record.appData.CMEmail;
                course.FacultyId = Int32.Parse(record.appData.CMFacultyID);
                course.FacultyName = record.appData.CMFaculty;
                course.SectionCode = Int32.Parse(record.appData.CMSectionCode);
                course.SectionTitle = record.appData.CMSectionTitle;

                list.Add(course);
            }

            return list;
        }

        /*
         *  getAttendanceByID() - This method retrieves the attendance records from Nextworld by Faculty id
         *                          *Note: This does not implement API paging by offset.
         *  
         *  @param id - The ID of the requesting faculty member.
         */ 
        public List<GetFacultyClassAttendanceTaken_Result> getAttendaceByID(Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();
            
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toCoursesByID(getRoot, id);

            return list;
        }

        /*
         *  getAttendanceByIDLoop() - This method retrieves the attendance records from Nextworld by Faculty id
         *                              *Note: This implements API paging by offset.
         *  
         *  @param id - The ID of the requesting faculty member.
         */
        public List<GetFacultyClassAttendanceTaken_Result> getAttendanceByIDLoop(Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();
            
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);
            
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);
            
            list = toCoursesByID(getRoot, id);

            //Loop through the grades api and concatenate all the grades for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_GRADES + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new AttendanceCourse.RootObject();
                getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response1.Content);

                list = list.Concat(toCoursesByID(getRoot, id)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        /*
         *  toCoursesByID() - This method translates an attendance record for a course from the model that corresponds to the JSON payload from
         *                      the simulation application in Nextworld to the model that will be used in the Attendance custom program.
         *                      
         *  @param root - The root object that contains all the attendance records from Nextworld.
         *  
         *  @param id - The Nextworld ID of the requesting faculty member.
         */ 
        public List<GetFacultyClassAttendanceTaken_Result> toCoursesByID(AttendanceCourse.RootObject root, Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            List<AttendanceCourse.Record> courses = root.Data.records;

            GetFacultyClassAttendanceTaken_Result course;
            AttendanceCourse.Record record;

            for (int i = 0; i < courses.Count(); i++)
            {
                course = new GetFacultyClassAttendanceTaken_Result();
                record = courses.ElementAt(i);

                if (record.appData.CMFacultyID.Equals(id))
                {
                    course.BlockColor = record.appData.CMBlockColor;
                    course.CourseCode = record.appData.CMCourseCode;
                    course.CourseName = record.appData.CMCourse;
                    course.FacultyEmail = record.appData.CMEmail;
                    course.FacultyId = Int32.Parse(record.appData.CMFacultyID);
                    course.FacultyName = record.appData.CMFaculty;
                    course.SectionCode = Int32.Parse(record.appData.CMSectionCode);
                    course.SectionTitle = record.appData.CMSectionTitle;

                    list.Add(course);
                }
            }
            return list;
        }
    }
}
