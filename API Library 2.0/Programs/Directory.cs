﻿using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Directory
    {
        //public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("integration.valor", "11Govalor01!");

        //Token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("integration.valor", "11Govalor01!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "ValorIntegration";
        private static string LIFECYCLE = "valor";


        public static NWDirectory.RootObject getDirectoryRecord(string nwid) // passing in null or an empty string for the nwid should casue it to download the whole thing
        {
            NWDirectory.RootObject temp = new NWDirectory.RootObject();

            //var client = new RestClient("https://master-api1.nextworld.net/v2/Directory/"+nwid);
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/Directory/" + nwid);
            var request = new RestRequest(Method.GET);
            //client.Authenticator = auth;

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            temp = JsonConvert.DeserializeObject<NWDirectory.RootObject > (response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return temp;            
        }

        public static NWDirectory.RootObject getDirectoryRecordByExternalID(string externalID)
        {
            NWDirectory.RootObject temp = new NWDirectory.RootObject();

            //var client = new RestClient("https://master-api1.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22nwExternalId%22:%7B%22$like%22:%22"+externalID+"%22%7D%7D%5D%7D");
            var client = new RestClient("https://api-o1p.prod.nextworld.net/v2/Directory?nwFilter=%7B%22$and%22:%5B%7B%22nwExternalId%22:%7B%22$like%22:%22" + externalID + "%22%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);
            //client.Authenticator = auth;

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            temp = JsonConvert.DeserializeObject<NWDirectory.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return temp;
        }
    }
}
