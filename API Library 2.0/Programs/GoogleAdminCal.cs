﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminCal
    {
        //table specific
        //public static string URL_BASE_S_SCHED = "https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_T_SCHED = "https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_TEACHERS = "https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_STUDENTS = "https://master-api1.nextworld.net/v2/JVGetStudentsResult?nwPaging=%7B%22limit%22:15,%22offset%22:";

        public static string URL_BASE_S_SCHED = "https://master-api1.nextworld.net/v2/StudentSchedule/";

        //general
        public static string URL_P2 = "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:";
        public static string URL_P3 = "%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_END = "%7D";

        //used for token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "AppStable";
        private static string LIFECYCLE = "jake_v";

        public static List<StudentModel> GetStudents()
        {
            //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
            List<StudentModel> temp = new List<StudentModel>();

            //allStudentsFromNW = getAllStudentsRootObjectAPI("https://master-api1.nextworld.net/v2/JVGetStudentsResult");
            temp = GoogleAdminUsers.GetStudents(); //api paging. Using the method from the other library because it is the exact same
            //logger.Debug("Converted the NW model to the old model");
            return temp;
        }


        // Old way with SQL
        //public virtual List<StudentScheduleModel> GetStudentSchedule(int id)
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
        //    return this.Database.SqlQuery<StudentScheduleModel>("[Schoology].[GetStudentSchedule] " + id).ToList();
        //}

        public static List<StudentScheduleModel> GetStudentSchedule(string nwid)
        {
            List<StudentScheduleModel> temp = new List<StudentScheduleModel>();

            //CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId);
            CoursesNWGET.RootObject root = generateStudentSchedule(nwid); //added api url looping

            if (root != null && root.Data != null && root.Data.records != null)
            {
                // get all of the appdatas in an arrary
                List<CoursesNWGET.AppData> appDatas = new List<CoursesNWGET.AppData>();

                for (int i = 0; i < root.Data.records.Count; i++)
                {
                    appDatas.Add(root.Data.records.ElementAt(i).appData);
                }

                // These are elements from other tables that I need to get from another API but need to define here for reasing with null objects
                StudentScheduleModel tempSchedule = new StudentScheduleModel();
                NWDirectory.RootObject teacher = new NWDirectory.RootObject();
                FacilitiesNW.RootObject room = new FacilitiesNW.RootObject();
                CourceDefinitionNW.RootObject courcedef = new CourceDefinitionNW.RootObject();


                foreach (CoursesNWGET.AppData appData in appDatas)
                {

                    CourceSectionsNW.RootObject tempSection = generateSection(appData.CourseSection); // the section contains things like the block color and the cource itself(which then contains name and code and stuff like that)
                    if (tempSection.Data != null)
                    {
                        teacher = Directory.getDirectoryRecord(tempSection.Data.records.ElementAt(0).appData.Teacher); // this gets the teacher
                        if (tempSection.Data.records.ElementAt(0).appData.RoomNumber != null)
                        {
                            room = generateRoom(tempSection.Data.records.ElementAt(0).appData.RoomNumber); // The room in in a different table (faculty)

                        }
                        if (tempSection.Data.records.ElementAt(0).appData.RoomNumber != null)
                        {
                            courcedef = genCourceDef(tempSection.Data.records.ElementAt(0).appData.Course); // cource def has the name
                        }
                    }
                    

                    tempSchedule = new StudentScheduleModel();
                    tempSchedule.BlockColor = appData.Block;

                    switch (appData.Block)
                    {
                        case "01":
                            tempSchedule.BlockColor = "Red";
                            break;
                        case "02":
                            tempSchedule.BlockColor = "Orange";
                            break;
                        case "03":
                            tempSchedule.BlockColor = "Yellow";
                            break;
                        case "04":
                            tempSchedule.BlockColor = "Green";
                            break;
                        case "05":
                            tempSchedule.BlockColor = "Columbia";
                            break;
                        case "06":
                            tempSchedule.BlockColor = "Navy";
                            break;
                        case "07":
                            tempSchedule.BlockColor = "Purple";
                            break;
                        case "08":
                            tempSchedule.BlockColor = "Grey";
                            break;
                        default:
                            tempSchedule.BlockColor = "Red";
                            break;
                    }
                    tempSchedule.BlockColorRGB = appData.Block; // I can not find anywhere this is used, so I am not too worried about it being wrong. Plus it would be like 3 more API calls to get it
                    if (courcedef != null && courcedef.Data != null)
                    {
                        tempSchedule.ClassName = courcedef.Data.records.ElementAt(0).appData.CourseName;

                    }
                    if (tempSection.Data != null)
                    {
                        tempSchedule.Course = tempSection.Data.records.ElementAt(0).appData.CourseCodeSection;

                        tempSchedule.CourseSection = tempSection.Data.records.ElementAt(0).appData.CourseCodeSection;
                    }
                    tempSchedule.CycleDaysID = appData.CycleBlock;
                    tempSchedule.Day = appData.Cycle;

                    switch (appData.Cycle)
                    {
                        case "1":
                            tempSchedule.Day = "Monday";
                            break;
                        case "2":
                            tempSchedule.Day = "Tuesday";
                            break;
                        case "3":
                            tempSchedule.Day = "Wednesday";
                            break;
                        case "4":
                            tempSchedule.Day = "Thursday";
                            break;
                        case "5":
                            tempSchedule.Day = "Friday";
                            break;
                        default:
                            tempSchedule.Day = "Monday";
                            break;
                    }
                    tempSchedule.EndTime = appData.PeriodEndTime.ToString();
                    tempSchedule.EndTimeRaw = DateTime.Today + new TimeSpan(0, 0, 0, 0, appData.PeriodEndTime);
                    tempSchedule.Faculty = teacher.Data.records.ElementAt(0).appData.Name.NameName;
                    tempSchedule.Room = room.Data.records.ElementAt(0).appData.FacilityName; // will need another api
                    tempSchedule.Semester = appData.Term;
                    tempSchedule.StartTime = appData.PeriodStartTime.ToString();
                    tempSchedule.StartTimeRaw = DateTime.Today + new TimeSpan(0, 0, 0, 0, appData.PeriodStartTime);
                    temp.Add(tempSchedule);
                }
            }



            return temp;
        }

        private static CourceDefinitionNW.RootObject genCourceDef(string nwId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/Courses?nwFilter=%7B%22$and%22:%5B%7B%22nwId%22:%7B%22$like%22:%22"+nwId+"%22%7D%7D%5D%7D"); //currently there is no API looping 
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CourceDefinitionNW.RootObject>(response.Content);
        }

        private static FacilitiesNW.RootObject generateRoom(string roomNumber)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/Facilities/" + roomNumber); //currently there is no API looping 
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<FacilitiesNW.RootObject>(response.Content);
        }

        private static CourceSectionsNW.RootObject generateSection(string courseSection)
        {
            if (courseSection != null)
            {
                var client = new RestClient("https://master-api1.nextworld.net/v2/CourseSections?nwFilter=%7B%22$and%22:%5B%7B%22Inactive%22:false%7D,%7B%22nwId%22:%7B%22$like%22:%22" + courseSection + "%22%7D%7D%5D%7D"); //currently there is no API looping 
                var request = new RestRequest(Method.GET);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                var response = client.Execute(request);

                return JsonConvert.DeserializeObject<CourceSectionsNW.RootObject>(response.Content);

            }
            return new CourceSectionsNW.RootObject();
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(string nwidOfStudent)
        {
            if (nwidOfStudent != null && !nwidOfStudent.Equals("")) // only runs if the nwID is not null or empty
            {
                var client = new RestClient("https://master-api1.nextworld.net/v2/StudentSchedule?nwFilter=%7B%22$and%22:%5B%7B%22AcademicYear%22:%222017-2018%22%7D,%7B%22Term%22:%22Spring%22%7D,%7B%22Student%22:%7B%22$like%22:%22"+nwidOfStudent+"%22%7D%7D%5D%7D"); //currently there is no API looping 
                var request = new RestRequest(Method.GET);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                var response = client.Execute(request);

                return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
            }

            return new CoursesNWGET.RootObject();
        }


        public static List<TeacherModel> GetTeachers()
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            //TeacherNW.RootObject root = getAllTeacherRootObject("https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult/");
            //TeacherNW.RootObject root = getAllTeacherRoot(); //api paging
            List<FacultyStaffModel> root = GoogleAdminUsers.GetFaculty();
            temp = convertRootObjectTeacherIntoModel(root);

            return temp;

        }

        private static List<TeacherModel> convertRootObjectTeacherIntoModel(List<FacultyStaffModel> root)
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            var records = root;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherRecordtoOldModel(records.ElementAt(i)));
            }

            return temp;
        }

        private static TeacherModel covertTeacherRecordtoOldModel(FacultyStaffModel appData)
        {
            TeacherModel temp = new TeacherModel();

            temp.Department = "Not Here";
            temp.EA7RecordsID = appData.nwID;
            temp.Email = appData.Email;
            temp.FacultyID = appData.FacultyID;
            temp.FirstName = appData.FirstName;
            temp.LastName = appData.LastName;
            temp.SchoolID = appData.FacultyID;

            return temp;
        }

        private static TeacherNW.RootObject getAllTeacherRootObject(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            TeacherNW.RootObject root = new TeacherNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }


        public static List<TeacherScheduleModel> GetTeacherSchedule(string eA7RecordsId)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();


            //TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId);
            TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId); //added looping

            temp = convertRootObjectTeacherScheduleIntoModel(root);
            if (temp.Count > 0)
            {
                Console.WriteLine("Yayyy");
            }
            return temp;
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(string eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/TeacherSchedule?nwFilter=%7B%22$and%22:%5B%7B%22Term%22:%22Spring%22%7D,%7B%22AcademicYear%22:%222017-2018%22%7D,%7B%22Teacher%22:%7B%22$like%22:%22"+eA7RecordsId+"%22%7D%7D%5D%7D&");
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }



        private static List<TeacherScheduleModel> convertRootObjectTeacherScheduleIntoModel(TeacherScheduleNW.RootObject root)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();

            List<TeacherScheduleNW.Record> records = new List<TeacherScheduleNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherScheduleRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static TeacherScheduleModel covertTeacherScheduleRecordtoOldModel(TeacherScheduleNW.AppData appData)
        {
            TeacherScheduleModel temp = new TeacherScheduleModel();
            CourceSectionsNW.RootObject section = new CourceSectionsNW.RootObject();
            CourceDefinitionNW.RootObject cource = new CourceDefinitionNW.RootObject();
            if (appData.CourseSection != null)
            {
                section = generateSection(appData.CourseSection);

            }
            if (section.Data != null)
            {
                cource = genCourceDef(section.Data.records.ElementAt(0).appData.Course);
            }

            temp.BlockColor = appData.Cycle;
            if (section.Data != null)
            {
                switch (section.Data.records.ElementAt(0).appData.Block)
                {
                    case "01":
                        temp.BlockColor = "Red";
                        break;
                    case "02":
                        temp.BlockColor = "Orange";
                        break;
                    case "03":
                        temp.BlockColor = "Yellow";
                        break;
                    case "04":
                        temp.BlockColor = "Green";
                        break;
                    case "05":
                        temp.BlockColor = "Columbia";
                        break;
                    case "06":
                        temp.BlockColor = "Navy";
                        break;
                    case "07":
                        temp.BlockColor = "Purple";
                        break;
                    case "08":
                        temp.BlockColor = "Grey";
                        break;
                    default:
                        temp.BlockColor = "Red";
                        break;
                }
            }
            else
            {
                temp.BlockColor = "Red"; // default so it is not null
                
            }
            //temp.BlockColorRGB = appData.JVBlockColorRGB;
            if (cource.Data != null)
            {
                temp.ClassName = cource.Data.records.ElementAt(0).appData.CourseName;
                temp.Course = cource.Data.records.ElementAt(0).appData.CourseCode;
                temp.CourseSection = cource.Data.records.ElementAt(0).appData.CourseCode;
            }
            temp.CycleDaysID = appData.Cycle;
            switch (appData.Cycle)
            {
                case "1":
                    temp.Day = "Monday";
                    break;
                case "2":
                    temp.Day = "Tuesday";
                    break;
                case "3":
                    temp.Day = "Wednesday";
                    break;
                case "4":
                    temp.Day = "Thursday";
                    break;
                case "5":
                    temp.Day = "Friday";
                    break;
                default:
                    temp.Day = "Monday";
                    break;
            }
            temp.EndTime = appData.PeriodEndTime.ToString();
            temp.EndTimeRaw = DateTime.Today + new TimeSpan(0, 0, 0, 0, appData.PeriodEndTime);
            temp.StartTime = appData.PeriodStartTime.ToString();
            temp.StartTimeRaw = DateTime.Today + new TimeSpan(0, 0, 0, 0, appData.PeriodStartTime);

            return temp;
        }
    }
}
// This is the looping api Cheyanne did, I have to change it to get it to call prod tables
//private static TeacherNW.RootObject getAllTeacherRoot()
//{
//    TeacherNW.RootObject root = new TeacherNW.RootObject();
//    List<TeacherNW.Record> data = new List<TeacherNW.Record>();

//    //initial request to get data from Nextworld
//    RestClient client = new RestClient(URL_BASE_TEACHERS + "0" + URL_END);
//    RestRequest request = new RestRequest(Method.GET);

//    //using a token bearer to authenticate request
//    ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//    client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//    client.AddDefaultHeader("Content-Type", "application/json");

//    IRestResponse response = client.Execute(request);

//    root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);

//    data = root.Data.records;

//    //Loop through the student api and concatenate all the schedules for later use
//    int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    String nextURL = "";
//    String nextOffset = "";

//    while (endTest < root.Data.pageData.total)
//    {
//        //find next url
//        nextOffset = endTest.ToString();

//        nextURL = URL_BASE_TEACHERS + nextOffset + URL_END;

//        //get the next set of data from Nextworld
//        client = new RestClient(nextURL);

//        //using a token bearer to authenticate request
//        ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//        client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//        client.AddDefaultHeader("Content-Type", "application/json");

//        IRestResponse response1 = client.Execute(request);

//        root = new TeacherNW.RootObject();
//        root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response1.Content);

//        data = data.Concat(root.Data.records).ToList();

//        //update the end test
//        endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    }

//    root = new TeacherNW.RootObject();
//    TeacherNW.Data rootData = new TeacherNW.Data();
//    rootData.records = data;
//    root.Data = rootData;

//    return root;
//}

// Old way with sql
//public virtual List<TeacherScheduleModel> GetTeacherSchedule(int id)
//{
//    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
//    return this.Database.SqlQuery<TeacherScheduleModel>("[Schoology].[GetTeacherSchedule] " + id).ToList();
//}


// Old way before switching to the real Table
//private static CoursesNWGET.RootObject generateStudentSchedule(int ea7)
//{
//    List<CoursesNWGET.Record> data = new List<CoursesNWGET.Record>();
//    CoursesNWGET.RootObject root = new CoursesNWGET.RootObject();

//    //initial request to get data from Nextworld
//    RestClient client = new RestClient(URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
//    RestRequest request = new RestRequest(Method.GET);

//    //using a token bearer to authenticate request
//    ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//    client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//    client.AddDefaultHeader("Content-Type", "application/json");

//    IRestResponse response = client.Execute(request);

//    root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);

//    data = root.Data.records;

//    //Loop through the student api and concatenate all the schedules for later use
//    int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    String nextURL = "";
//    String nextOffset = "";

//    while (endTest < root.Data.pageData.total)
//    {
//        //find next url
//        nextOffset = endTest.ToString();

//        nextURL = URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

//        //get the next set of data from Nextworld
//        client = new RestClient(nextURL);

//        //using a token bearer to authenticate request
//        ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//        client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//        client.AddDefaultHeader("Content-Type", "application/json");

//        IRestResponse response1 = client.Execute(request);

//        root = new CoursesNWGET.RootObject();
//        root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response1.Content);

//        data = data.Concat(root.Data.records).ToList();

//        //update the end test
//        endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    }

//    root = new CoursesNWGET.RootObject();
//    CoursesNWGET.Data rootData = new CoursesNWGET.Data();
//    rootData.records = data;
//    root.Data = rootData;

//    return root;
//}

// Old way with SQL
//public virtual List<TeacherModel> GetTeachers()
//{
//    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
//    return this.Database.SqlQuery<TeacherModel>("[Schoology].[GetTeachers]").ToList();
//}

    // Looped api, needed to change for prod
//private static TeacherScheduleNW.RootObject generateTeacherScheduleLoop(int ea7)
//{
//    TeacherScheduleNW.RootObject root = new TeacherScheduleNW.RootObject();
//    List<TeacherScheduleNW.Record> data = new List<TeacherScheduleNW.Record>();

//    //initial request to get data from Nextworld

//    //change this
//    RestClient client = new RestClient(URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
//    RestRequest request = new RestRequest(Method.GET);

//    //using a token bearer to authenticate request
//    ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//    client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//    client.AddDefaultHeader("Content-Type", "application/json");

//    IRestResponse response = client.Execute(request);

//    root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);

//    data = root.Data.records;

//    //Loop through the student api and concatenate all the schedules for later use
//    int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    String nextURL = "";
//    String nextOffset = "";

//    while (endTest < root.Data.pageData.total)
//    {
//        //find next url
//        nextOffset = endTest.ToString();

//        nextURL = URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

//        //get the next set of data from Nextworld
//        client = new RestClient(nextURL);

//        //using a token bearer to authenticate request
//        ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
//        client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
//        client.AddDefaultHeader("Content-Type", "application/json");

//        IRestResponse response1 = client.Execute(request);

//        root = new TeacherScheduleNW.RootObject();
//        root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response1.Content);

//        data = data.Concat(root.Data.records).ToList();

//        //update the end test
//        endTest = root.Data.pageData.offset + root.Data.pageData.limit;
//    }

//    root = new TeacherScheduleNW.RootObject();
//    TeacherScheduleNW.Data rootData = new TeacherScheduleNW.Data();
//    rootData.records = data;
//    root.Data = rootData;

//    return root;
//}