﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    /*
     *  GetGroups_Result is the model that is used in SchoologyEligibility class as well as in the Schoology Reports - Eligibility custom program.
     *  
     *  This corresponds to the data format that is needed in the Schoology Reports solution.
     */
    public class GetGroups_Result
    {
        public string groupid { get; set; }
        public string groupname { get; set; }
    }
}
