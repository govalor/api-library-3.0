﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class LeaderFundDetail
    {
        public int LeaderId { get; set; }
        public decimal TotalFunds { get; set; }
        public string SolicitorId { get; set; }
        public string SolicitorFirstName { get; set; }
        public string SolicitorLastName { get; set; }
        public int ExperienceId { get; set; }
        public string ExperienceDescription { get; set; }
        public decimal GiftTotal { get; set; }
        public int LineItemId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public short DonorAnonymous { get; set; }
        public short AmountAnonymous { get; set; }
        public string Contributor { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string CityStateZip { get; set; }
        public string ContributorNotes { get; set; }
    }
}
