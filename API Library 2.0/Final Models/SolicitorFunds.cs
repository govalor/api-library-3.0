﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class SolicitorFunds : IEquatable<SolicitorFunds>
    {
        public int SolicitorId { get; set; }
        public int FundId { get; set; }
        public string Description { get; set; }

        public bool Equals(SolicitorFunds other)
        {
            return other.SolicitorId.Equals(SolicitorId) && other.FundId.Equals(FundId) && other.Description.Equals(Description);
        }

        public override int GetHashCode()
        {
            int hashSolicitorId = SolicitorId == null ? 0 : SolicitorId.GetHashCode();
            int hashFundId = FundId == null ? 0 : FundId.GetHashCode();
            int hashDescription = Description == null ? 0 : Description.GetHashCode();

            return hashSolicitorId ^ hashFundId ^ hashDescription;
        }
    }


}
