﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{

    public class TeacherScheduleModel
    {
        public string Day { get; set; }
        public string Course { get; set; }
        public string CourseSection { get; set; }
        public string ClassName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime StartTimeRaw { get; set; }
        public DateTime EndTimeRaw { get; set; }
        public string BlockColor { get; set; }
        public string BlockColorRGB { get; set; }
        public string Room { get; set; }
        public string CycleDaysID { get; set; }
    }
}
