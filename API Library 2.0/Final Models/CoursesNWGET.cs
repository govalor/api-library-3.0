﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    class CoursesNWGET
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string Term { get; set; }
            public string Block { get; set; }
            public string Cycle { get; set; }
            public string Student { get; set; }
            public bool znwLocked { get; set; }
            public string CycleBlock { get; set; }
            public string AcademicYear { get; set; }
            public string CourseSection { get; set; }
            public int PeriodEndTime { get; set; }
            public int PeriodStartTime { get; set; }
            public int ScheduleSequence { get; set; }
            public string ScheduledEventType { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string ScheduledEventName { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }

    }
