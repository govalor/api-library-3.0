﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class LeaderFunds : IEquatable<LeaderFunds>
    {
        public int LeaderId { get; set; }
        //public string LeaderName { get; set; }
        public int FundId { get; set; }
        public string ExperienceDescription { get; set; }

        public bool Equals(LeaderFunds other)
        {
            return other.LeaderId.Equals(LeaderId) && other.FundId.Equals(FundId) && other.ExperienceDescription.Equals(ExperienceDescription);
        }

        public override int GetHashCode()
        {
            int hashSolicitorId = LeaderId == null ? 0 : LeaderId.GetHashCode();
            int hashFundId = FundId == null ? 0 : FundId.GetHashCode();
            int hashDescription = ExperienceDescription == null ? 0 : ExperienceDescription.GetHashCode();

            return hashSolicitorId ^ hashFundId ^ hashDescription;
        }
    }

}
