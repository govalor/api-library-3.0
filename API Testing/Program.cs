﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_Library_2._0;

namespace API_Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            SchoolDirectory directory = new SchoolDirectory();

            List<DirectoryNW2.Record> studentList = directory.getDirectoryRecords();

            List<string> ids = directory.getStudentNWIDs(studentList);

            foreach(string id in ids)
            {
                Console.WriteLine(id);
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
